\select@language {spanish}
\contentsline {section}{\numberline {1}Usurarios, grupos y permisos}{1}{section.1}
\contentsline {subsection}{\numberline {1.1}Cambien usuario a root}{1}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Comando adduser}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Comando mkdir}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Comando nano}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Comando chmod}{3}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Comando addgroup}{3}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}Usermod}{4}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Comando chgrp}{4}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Discusi\IeC {\'o}n}{4}{subsection.1.9}
\contentsline {section}{\numberline {2}Instalaci\IeC {\'o}n de paquetes y programas dese otros repositorios}{5}{section.2}
\contentsline {section}{\numberline {3}Cron, crontab,rsync,ssh}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}Ssh y rsync}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Cron y Crontab}{7}{subsection.3.2}
\contentsline {section}{\numberline {4}Instalaci\IeC {\'o}n de programas desde c\IeC {\'o}digo fuente}{8}{section.4}
